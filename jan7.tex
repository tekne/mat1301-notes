\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1301 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 7 2020}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{mathabx}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}

\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{{\frac{\partial {#1}}{\partial {#2}}}}
\def\ries{{\hat{\mbb{C}}}}
\newcommand{\reals}{\mbb{R}}
\newcommand{\incl}{\xhookrightarrow{}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\Id}{id}

\begin{document}

\maketitle

In algebraic topology, we assign to a space \(X\) an algebraic invariant \(F(X)\), usually a group, in such a way that the assignment is factorial, i.e., given homeomorphisms \(f: X \to Y\), \(g: Y \to Z\), we can obtain an isomorphism \(f_*: F(X) \to F(Y)\), \(g_*: F(Y) \to F(Z)\), satisfying
\[\Id_* = \Id, \qquad (g \circ f)_* = g_* \circ f_*\]
That's broadly speaking what algebraic topoology is. Now, the nature of this group can be very different, i.e. homotopy groups, cohomology, etc., but they all broadly speaking fit into this picture. In this course we will mainly be deailing with homotopy groups, homology and cohomology, which corresponds to the first three chapters of the textbook.

We begin by noting that all maps here are continuous, but not necessarily smooth.
\begin{definition}[Homotopy]
\(f, g: X \to Y\) are said to be \textbf{homotopic} (written \(f \sim g\)) if
\[\exists: F: X \times I \to Y, \forall x \in X, F(x, 0) = f(x) \land F(x, 1) = g(x)\]
We often write \(f_t(x) = F(x, t)\). \(F\) is called a \textbf{homotopy} from \(f\) to \(g\).
\end{definition}
For example, if \(f, g: X \to C \subseteq \reals^n\), where \(C\) is convex, for all points \(a, b \in C\), the line between \(a\) and \(b\) lies in \(C\). Hence, we can define the \textbf{straight-line homotopy}
\[F(x, t) = (1 - t)f(x) + tg(x)\]
This can be used in other cases, for example \(f, g: X \to \reals^n \setminus \{0\}\). We have that
\[|f(x) - g(x)| < |f(x)| \implies f \sim g\]
by the straight line homotopy. We'll see later that not all maps from \(X \to \reals^n \setminus \{0\}\) which are not homotopic, but at the moment we don't know any.

Homotopy is very easily seen to be an equivalence relation, meaning that
\[f \sim g \implies g \sim f, \qquad f \sim f, \qquad f \sim g \land g \sim h \implies f \sim h\]
\begin{definition}[Null homotopic]
We say that \(f\) is \textbf{null homotopic} if \(f \sim g\), where \(g\) is constant.
\end{definition}
If \(Y \subseteq \reals^n\) is convex, then any map \(f: X \to Y\) is null homotopic. This is not always true, however. For example, we'll see that the inclusion map \(S^1 \incl \reals^2 \setminus 0\), \(f\) is \textit{not} null homotopic. We don't know why yet, but we'll prove it later using the fundamental group. The problem is of course that we removed 0, as otherwise we could use the straight line homotopy.
For another example, defining \(S^1 \subseteq \{|z| = 1\} \subseteq \mbb{C}\), taking \(f\) to be the inclusion map \(S^1 \incl \mbb{C} \setminus \{0\}\), and \(g: S^1 \to \mbb{C} \setminus 0\) given by \(g(z) = z^2\), we'll later see that \(f \not\sim g\).

The notion of homotopy also implies the notion of homotopy equivalent spaces.
\begin{definition}[Homotopy equivalence]
\(X, Y\) are called \textbf{homotopy equivalent}, written \(X \sim Y\) if
\[\exists f: X \to Y, g: Y \to X, g \circ f \sim \Id \land f \circ g \sim \Id\]
\end{definition}
Note that, in the case of homeomorphism, we ask for these compositions to be \textit{equal} to the identity, where here we merely require that they are homotopic. Again, we find that homotopy equivalence is an equivalence relation.

\begin{definition}[Contractible]
\(f\) is called \textbf{contractible} if \(f\) is homotopy equivalent to a point.
\end{definition}
An example that we know is that if \(X \subset \reals^n\) is convex (it can be compact or not) then it is contractible.

\begin{definition}[Retraction]
Let \(A \subset X\). We say that \(r: X \to A\) is a \textbf{retraction} if \(r|_A = \Id\).
\end{definition}

\begin{definition}[Deformation retraction]
Let \(A \subset X\) and \(r: X \to A\) be a retraction. \(r\) is called a \textbf{deformation retraction} if \(r \sim \Id_X\) via a homotopy \(f_t(x)\) such that each \(f_t\) is a retraction, i.e.
\[\forall t \in I, a \in A, f_t(a) = a\]
\end{definition}
One thing to observe of course is that if \(r: X \to A\) is a deformation retraction then \(r\) is a homotopy equivalence, as we trivially have \(r \circ \Id = \Id\), and by definition \(\Id \circ r \sim \Id\).

\end{document}
