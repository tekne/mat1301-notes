\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1301 Summary}
\author{Jad Elkhaleq Ghalayini}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{mathabx}
\usepackage{xcolor}

\usepackage[margin=1in]{geometry}

\usepackage{hyperref}
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}

\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{{\frac{\partial {#1}}{\partial {#2}}}}
\def\ries{{\hat{\mbb{C}}}}
\newcommand{\reals}{\mbb{R}}
\newcommand{\incl}{\xhookrightarrow{}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem*{corollary}{corollary}

\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\Id}{id}
\DeclareMathOperator{\RP}{RP}

\begin{document}

\maketitle

This is a summary of Hatcher's \textit{Algebraic Topology}, written to study for MAT1301 at the University of Toronto. The goal is to summarize each section of the book covered in the course, but detail may vary significantly based off what I consider hard and how much time I have. There may also be mistakes. Lots of them. You have been warned.

\tableofcontents

\newpage

\section{Underlying Notions}

\subsection{Homotopy and Homotopy Type}

In topology, the natural notion of ``equivalent" spaces is ``homeomorphic." Algebraic topology broadens this notion to ``homotopic" spaces. To develop this notion, we begin by defining a homotopy"
\begin{definition}[Homotopy]
A \textbf{homotopy} between two functions \(f_0, f_1: X \to Y\) is a family of functions, for \(t \in [0, 1]\), \(f_t: X \to Y\) such that the map
\[F: X \times [0, 1] \to X, \qquad F(x, t) = f_t(x)\]
is continuous.
In this case, we say \(f_0, f_1\) are \textbf{homotopic}, written \(f_0 \simeq f_1\)
\end{definition}
Now that we know what it means for functions to be homotopic, we must determine what it means for spaces to be homotopic. One example which we'd like to hold is the following: a thick ring can be continuously shunk down into a circle, but these spaces are most certainly not homeomorphic. Such examples lead to the following definition:
\begin{definition}[Deformation Retraction]
A \textbf{deformation retraction} of a space \(X\) onto a subset \(A \subseteq X\) is a family of maps, for \(t \in [0, 1]\), \(f_t: X \to X\) such that \(f_0 = \Id, f_1(X) = A\) and \(\forall t \in [0, 1], f_t|_A = \Id\) such that the map
\[F: X \times [0, 1] \to X, \qquad F(x, t) = f_t(x)\]
is continuous.
\end{definition}
In other words, a deformation retraction is a homotopy between the identity and some map \(f: X \to A \subseteq X\) such that each map \(f_t\) in the homotopy fixes \(A\) \textit{pointwise}. Some examples include:
\begin{itemize}

  \item A deformation retraction of a Möbius band onto its core circle, by scaling the distance of points on the square quotiented to obtain the band from the center by \(t\).

  \item A deformation retraction of a plane with two holes in it into an infinity sign.

\end{itemize}
We can now define homotopy equivalence in general:
\begin{definition}[Homotopy equivalence]
A map \(f: X \to Y\) is called a \textbf{homotopy equivalence} if there exists a map \(g: Y \to X\) such that \(f \circ g \simeq \Id\), \(g \circ f \simeq \Id\). In this case, the spaces \(X, Y\) are said to be \textbf{homotopy equivalent}, or to have the same \textbf{homotopy type}, written \(X \simeq Y\)
\end{definition}
If \(X\) deformation retracts to \(A \subseteq X\), then \(X \simeq A\).
In general, \(X \simeq Y\) if and only if there exists a third space \(Z\) which deformation retracts to both \(X\) and \(Y\). We define some associated terminology:
\begin{definition}[Contractible]
A space \(X\) having the homotopy type of a single point is called \textbf{contractible}.
\end{definition}
\begin{definition}[Nullhomotopic]
A function \(f: X \to Y\) is \textbf{nullhomotopic} if it is homotopic to a constant map.
\end{definition}
In general, \(X\) is contractible if and only if \(\Id: X \to X\) is nullhomotopic.

\subsection{Cell Complexes}

One familiar method to make a torus is to identify opposite edges of a square (imagine rolling up the square into a cigar, and then joining the ends to make a donut). In general, we can make an orientable surface \(M_g\) of genus \(g\) by identifying the pairs of edges of a \(4g\)-gon. These \(4g\) edges become a union of \(2g\) circles in the surface, all intersecting at a single point (the reader may identify this as the wedge product \(\bigwedge_{i = 1}^{2g}S^1\)). The interior of the polygon, then, can be thought of as an open disk, or \textit{2-cell}, attached to the union of the circles.

The union of circles itself can be viewed as obtained from their common point of intersection by attacing \(2g\) open arcs, or \textit{1-cells}. So we can imagine building up the surface in stages: start with a point (a \textit{0-cell}), add \(2g\) arcs (1-cell) and then attach an open disk (2-cell)

We can greatly generalize this type of construction to define a \textit{cell complex}:
\begin{definition}[Cell Complex]

We construct a space \(X\) as follows:

\begin{enumerate}

  \item Begin with a discrete set of points (\textbf{0-cells}) \(X^0\)

  \item Inductively, form an \textbf{n-skeleton} \(X^n\) from \(X^{n - 1}\) by ``attaching" \textbf{\(n\)-cells} \(e_\alpha^n\) via maps \(\varphi_\alpha: S^{n - 1} \to X^{n - 1}\).

  That is, we define \(X^n\) to be the quotient space of the disjoint union,
  where each \(D_\alpha^n\) is a \underline{closed} \(n\)-disk,
  \[X^{n - 1} \sqcup \bigsqcup_\alpha D_\alpha^n\]
  under the identifications \(x \sim \varphi_\alpha(x)\) for \(x \in \partial D_\alpha^n = S^{n - 1}\). As a set, we can hence write
  \[X^n = X^{n - 1} \sqcup \bigsqcup_\alpha e_\alpha^n\]
  where each \(e_\alpha^n\) is an \underline{open} \(n\)-disk.

  \item Either stop at \(n\), setting \(X = X^n\), or continue infinitely, setting \(X = \bigcup_nX^n\) and assigning \(X\) the weak topology (i.e. \(A \subseteq X\) is open/closed iff \(A \cap X^n\) is open/closed for every \(n\))

\end{enumerate}
Such a space \(X\) is called a \textbf{cell complex} or \textbf{CW complex}.
If \(X = X^n\) for some \(n\), we call \(X\) \textbf{finite dimensional}, and the smallest such \(n\) is called the \textbf{dimension} of \(X\) (the maximum dimension of the cells of \(X\)).
\end{definition}

Let's consider some examples to get a feel for the concept:

\begin{enumerate}

  \item A 1-dimensional cell-complex \(X = X^1\) is a graph: a collection of points, some of which have edges between them (the vertices \(X^0\) being quotiented to the ends \(S^0 = \{0, 1\}\) of intervals \(D^1 = [0, 1]\), the edges). Note we allow loops, where the two ends of an edge are attached to the same vertex. This of course yields a copy of \(S^1\) (asumming no other vertices or edges).

  \item As stated above for \(S^1\), the sphere \(S^n\) can be written as a cell complex with just two cells: a point \(e^0\) and an \(n\)-cell \(e^n\), with the \(n\)-cell attached by the constant map \(e^0\).

  In other words, we identify every point on the boundary of \(D^n\) with all other points on the boundary, ``bunching it up" into \(S^n\). In the case of \(D^1\), we're wrapping up an interval into a circle. In the case of \(D^2\), we're folding a square into a pyramind, which is homeomorphic to a circle. Et cetera. Mathematically, we're regarding \(S^n\) as \(D^n / \partial D^n\).

\end{enumerate}

TODO: rest

\subsection{Operations on Spaces}

Cell complexes strike a balance between rigidity and flexibility: being rigid enough to allow a variety of arguments to proceed in a combinatorial, cell-by-cell fashion while simultaneosuly being flexible enough to allow a variety of natrual constructions to be performed on them. In this section we will describe and analyze some of these constructions:

\subsubsection{Products}

If \(X, Y\) are cell complexes, then \(X \times Y\) can be constructed as a cell complex with cells \(e_\alpha^m \times e_\beta^m\), with \(e_\alpha^m\) ranging over the cells of \(X\) and \(e_\beta^m\) ranging over the cells of \(Y\).

For example, if we write the torus as \(S^1 \times S^1\), and write the circle as the cell complex \(e_0 + e_1\), then the cell structure of the torus described at the beginning of this section, namely two ...

TODO: example

\subsubsection{Quotients}

\begin{definition}[Subcomplex]
If \(X\) is a cell complex and \(A \subset X\) is a cell complex made out of a subset of the cells of \(X\) (TODO: check), we call \(A\) a \textbf{subcomplex} of \(X\)
\end{definition}
If \((X, A)\) is a pair consisting of a cell complex \(X\) and a subcomplex \(A\) of \(X\), then the quotient space \(X/A\) inherits a cell complex structure from \(X\). Specifically, the cells of \(X/A\) are the cells of \(X\setminus A\) plus a single 0-cell corresponding to the image of \(A\) in \(X/A\). If a cell \(e_\alpha^n\) in \(X\setminus A\) is attached by \(\varphi_\alpha: S^{n - 1} \to X^{n - 1}\), then this same cell is attached by \(\varphi_\alpha \circ q\) in \(X/A\),
where \(q: X \to X/A\) is the quotient map.

TODO: example

\subsubsection{Suspension}

\begin{definition}[Suspension]
Given a space \(X\), the \textbf{suspension} \(SX\) is the quotient \(X \times I / (X \times \{0\}) / (X \times \{1\})\), i.e. the quotient of \(X \times I\) such that \(X \times \{0\}\) is collapsed to a point and \(X \times \{1\}\) is collapsed to another point.
\end{definition}
\begin{definition}[Cone]
TODO: this
\end{definition}

\subsubsection{Join}

\subsubsection{Wedge Product}

\subsubsection{Smash Product}

\subsection{Two Criteria for Homotopy Equivalence}

\subsection{The Homotopy Extension Property}

\section{Fundamental Groups}

\subsection{Basic Constructions}

\subsubsection{Paths and Homotopy}

\subsubsection{The Circle}

\subsubsection{Induced Homomorphisms}

\subsection{Van Kampen's Theorem}

\end{document}
